# DevConf.US 2019 - Streaming Functions as a Service with Apache Spark and Kubernetes

This repo contains the artifacts that I will be demonstrating in my talk for
devconf.us 2019. You can see the full schedule listing here:
[Streaming Functions as a Service with Apache Spark and Kubernetes](https://devconfus2019.sched.com/event/SKZ8/streaming-functions-as-a-service-with-apache-spark-and-kubernetes)

All source code is licensed under the Apache version 2 license, see [https://opensource.org/licenses/Apache-2.0](https://opensource.org/licenses/Apache-2.0).

The content in `presentation.pdf` is licensed under a CC BY-SA 4.0 license, see [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/).

## How to run the demo

### Prerequisites

* Access to an OpenShift cluster.

* The OpenShift client(`oc`) available in a terminal session, with an active
  login to the OpenShift cluster.

* A web browser with access to the OpenShift console available.

### Procedure

1. Create a new project for your work. For this documentation the name of the
   project will be `myproject`, you may choose any value for the project name.
   ```bash
   oc new-project myproject
   ```
1. Install Apache Kafka into the project. For Kafka you will use the
   [Strimzi project](https://strimzi.io). The 0.1.0 version is
   chosen here for its ease of deployment with basic access to OpenShift.
   *Note: this process may take a few minutes to complete, you might see
   warnings or errors from OpenShift during the process.*
   ```bash
   oc create -f https://raw.githubusercontent.com/strimzi/strimzi-kafka-operator/0.1.0/kafka-inmemory/resources/openshift-template.yaml
   oc new-app strimzi
   ```
1. Install the radanalytics.io manifest. The
   [radanalytics.io community](https://radanalytics.io) contains several
   projects focused on deploying Apache Spark in OpenShift. This manifest
   will enable those projects in OpenShift.
   ```bash
   oc create -f https://radanalytics.io/resources.yaml
   ```
1. Deploy the number generator service. This command will deploy the service
   that will generate messages on Kafka.
   ```bash
    oc new-app centos/python-36-centos7~https://gitlab.com/elmiko/kafka-number-generator.git \
      -e KAFKA_BROKERS=kafka:9092 \
      -e KAFKA_TOPIC=numbers \
      --name=generator
   ```
1. Deploy the filtering service. This command will deploy the service to
   filter out even numbers and broadcast them to a new topic, it will also
   automatically deploy an Apache Spark cluster bound to the service.
   ```bash
   oc new-app --template=oshinko-python36-spark-build-dc \
     -p APPLICATION_NAME=filter \
     -p GIT_URI=https://gitlab.com/bones-brigade/kafka-spark-python.git \
     -e KAFKA_BROKERS=kafka:9092 \
     -e KAFKA_IN_TOPIC=numbers \
     -e KAFKA_OUT_TOPIC=output \
     -p SPARK_OPTIONS='--packages=org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.0' \
     -e USER_FUNCTION_URI='https://gitlab.com/elmiko/devconf.us-2019-streaming-functions/raw/master/filters/evens.py'
   ```
1. Deploy listening service to monitor the output topic. This service will
   print messages from the specified topic to its logs. You can monitor the
   stream activity by following those logs.
   ```bash
   oc new-app centos/python-36-centos7~https://gitlab.com/bones-brigade/kafka-python-listener.git \
     -e KAFKA_BROKERS=kafka:9092 \
     -e KAFKA_TOPIC=output \
     --name=listener
   ```
1. Follow the logs of the listener. You should now see a stream of even
   numbers listed in the logs of the listener.
   ```bash
   oc logs -f dc/listener
   ```
1. Change the filter function to use the odd number filter. There are a few
   different methods for this, you will need to change the `USER_FUNCTION_URI`
   environment variable either by editing the deployment config with `oc edit dc/filter`
   or by navigating to the deployment configuration inside the OpenShift console.
   Regardless of the method, change the environment variable to `https://gitlab.com/elmiko/devconf.us-2019-streaming-functions/raw/master/filters/odds.py`.
1. Verify that the new filter is running by inspecting the logs of the listener.
   After the filter has redeployed you should start to see only odd numbers in
   the log output.

